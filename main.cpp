#include <cmath>
#include <memory>
#include <stdexcept>

#include "common.hpp"


bool gameEnded(const Bitstring& game, size_t wordLen)
{
    if (game.size() <= wordLen) return false;
    for (size_t i = 0; i + wordLen < game.size(); ++i)
    {
        if (std::equal(game.end() - wordLen, game.end(), game.begin() + i)) return true;
    }
    return false;
}

// Returns:
// false -> A has a winning strategy.
// true  -> B has a winning strategy.
bool whoHasWinningStrategy(Bitstring& game, char wordLen, char symCount = 2)
{
    // Since each player plays exactly one symbol each turn, the parity of the
    // game length (number of symbols played so far) will give us which player
    // should play the next symbol -- if it is even, (=0 mod 2) then the current
    // player is A, otherwise the current player is B.
    bool currPlayer = game.size() % 2;
    // *IF* the game has ended, it did when the preceeding player placed its
    // symbol. Therefore the preceeding player lost and the one before that won.
    // But as there are only two players, that player is the current one.
    if (gameEnded(game, wordLen)) return currPlayer;
    // The player now has to choose between the different symbols.
    for (char i = 0; i < symCount; ++i)
    {
        // Try placing one of them and test it.
        TryPlacing move(game, i);
        if (whoHasWinningStrategy(game, wordLen, symCount) == currPlayer)
        {
            // In this case, we have that the resulting game gives the current
            // player a winning strategy.
            return currPlayer;
        }
        // However if we didn't get a winning strategy that does NOT imply the
        // other player has a winning strategy -- it would be possible to get a
        // winning game if we place a different symbol. So that is what we do.

        // Note that at the end of this loop there's an implicit removal of the
        // speculatively placed symbol.
    }
    // Now we have exhausted all possible symbols we could place, and we were
    // never able to get a winning strategy. This must mean that the other
    // player has one instead.
    return !currPlayer;
}

// Return value: Whether the strategy will always make the given player win.
// Note: The strategy MUST be deterministic, otherwise the proof will be
// invalid.
bool proveStrategy(Bitstring& game, size_t wordLen,
                   const Strategy* strategy, bool player)
{
    bool currPlayer = game.size() % 2;
    if (gameEnded(game, wordLen))
    {
        return currPlayer == player;
    }
    if (currPlayer == player)
    {
        TryPlacing thisPlayerMove(game, (*strategy)(game, wordLen));
        return proveStrategy(game, wordLen, strategy, player);
    }
    for (char i = 0; i < 2; ++i)
    {
        TryPlacing otherPlayerMove(game, i);
        if (!proveStrategy(game, wordLen, strategy, player))
        {
            return false;
        }
    }
    return true;
}

struct ExtendedProofInfo
{
    size_t wordLen;
    size_t gamesWon;
    size_t totalGames;
    Bitstring game;
    const Strategy* strategy;
    bool player;
};

std::ostream& operator<<(std::ostream& ost, const ExtendedProofInfo& epi)
{
    std::string fmt;
    double percentage = 100.0 * (epi.gamesWon / (double)epi.totalGames);
    if (percentage < 40) /* Don't change format. */;
    else if (percentage < 80) fmt += fgYellow();
    else if (percentage < 90) fmt += fgPurple();
    else if (percentage < 99) fmt += bold() + fgGreen();
    else fmt += bold() + fgPurple();
    if (epi.gamesWon == epi.totalGames) fmt = bold() + fgRed();

    ost << "(won " << epi.gamesWon << " of " << epi.totalGames
        << " games, " << fmt << percentage << "%" << resetFmt() << ")";
    return ost;
}

// Return value: Whether the strategy will always make the given player win.
// Note: The strategy MUST be deterministic, otherwise the proof will be
// invalid.
// Note: This function is NOT very optimised (it will test all possible games to
// collect winning statistics), so it is not suitable for proving whether a
// strategy is a winning one in general.
bool proveStrategy(ExtendedProofInfo* info)
{
    bool currPlayer = info->game.size() % 2;
    if (gameEnded(info->game, info->wordLen))
    {
        // If the game has ended then the player just before have won the game.
        // As there are only two players this is equivalent to saying that the
        // player 'after' the losing player is the winner.
        if (currPlayer == info->player) ++info->gamesWon;
        ++info->totalGames;
        return currPlayer == info->player;
    }
    if (currPlayer == info->player)
    {
        // Current player only plays according to strategy.
        TryPlacing thisPlayerMove(info->game,
                                  (*info->strategy)(info->game, info->wordLen));
        return proveStrategy(info);
    }
    bool isCurrentWinner = true;
    // We test all the possible moves the other player can make to see if one of
    // them can somehow cause the current player to lose.
    for (char i = 0; i < 2; ++i)
    {
        TryPlacing otherPlayerMove(info->game, i);
        if (!proveStrategy(info))
        {
            isCurrentWinner = false;
        }
    }
    return isCurrentWinner;
}

// This tests whether performing the moves prescribed by the strategy will still
// cause the given player to have a winning strategy. For example for n=3 we
// know that B has a winning strategy, but does this still hold if player B
// uses a specific strategy for some of its moves? For example, what if B
// always placed a zero in its first move?
// IMPORTANT: useStrategy MUST contain at least ceil(2^n+n) elements, where
// n is the word length. Equivalently at least 2^(n-1) + ceil(n/2) elements.
// That array tells at which moves the strategy should be used -- for example if
// player=B and useStrategy={0,0,1,0,1,0,0,...,0} then the strategy will be used
// whenever B makes its third and fifth move, which will be the sixth and tenth
// move overall.
// Note that this also takes two compile-time parameters. This is done to allow
// the compiler to remove any unnecessary comparisons and reduce the number of
// parameters to the function.
template <size_t debugUntil = 0, bool includeExpected = false>
bool isPartOfWinningStrategy(Bitstring& game, size_t wordLen,
                             const Strategy* strategy, bool player,
                             bool useStrategy[])
{
    size_t move = game.size()/2;
    bool currPlayer = game.size() % 2;
    if (gameEnded(game, wordLen))
    {
        return currPlayer == player;
    }
    if (useStrategy[move] && currPlayer == player)
    {
        auto move = (*strategy)(game, wordLen);
        TryPlacing thisPlayerMove(game, move);
        return isPartOfWinningStrategy<debugUntil, includeExpected>
                                      (game, wordLen, strategy, player, useStrategy);
    }

    bool didItWork = false;
    for (char i = 0; i < 2; ++i)
    {
        TryPlacing playerMove(game, i);
        if (currPlayer == player)
        {
            if (whoHasWinningStrategy(game, wordLen) != player) continue;
        }
        if (!isPartOfWinningStrategy<debugUntil, includeExpected>
                                    (game, wordLen, strategy, player, useStrategy))
        {
            // If someone other than ourselves can make us lose then we do NOT
            // have a winning strategy.
            if (currPlayer != player)
            {
                return false;
            }
            // Otherwise, we may still have a winning strategy if we place
            // another symbol. So wo just continue the loop and hope for the
            // best.
        }
        else
        {
            // If we make a move which gives us a winning strategy, we then know
            // the given configuration gives us a winning strategy.
            if (currPlayer == player)
            {
                if (game.size() < debugUntil)
                {
                    didItWork = true;
                    bool expectedMove = move % 2;
                    Bitstring truncGame = game;
                    truncGame.pop_back();
                    bool mandatory = false;
                    if (!includeExpected && game.back() == expectedMove) continue;

                    {
                        TryPlacing _(truncGame, expectedMove);
                        if (whoHasWinningStrategy(truncGame, wordLen))
                        {
                            mandatory = true;
                        }
                    }
                    if (mandatory)
                    {
                        std::cout << bold() << fgRed() << "MUST choose "
                                  << (int)game.back() << " when handling game "
                                  << truncGame;
                        if (causesGameEnd(expectedMove, truncGame, wordLen))
                            std::cout << "  *";
                        std::cout << resetFmt() << "\n";
                    }
                    else
                    {
                        std::cout << "Given " << truncGame << " choosing " << (int)game.back() << " works.\n";
                    }
                    continue;
                }
                return true;
            }
        }
    }
    if (didItWork) return true;
    // In this case, the following may have happened:
    // * It is the opponent's turn, and he could not make any move which caused
    //   us to lose, so we have a winning strategy.
    // * It is our turn, and we could not make a move that caused us to have a
    //   winning strategy.
    // Therefore when reaching this point we have a winning strategy if and only
    // if it is the opponent's turn.
    return currPlayer != player;
}

// Simulates a game using any strategies (including probabilistic ones) for A
// and B. Returns whether B wins, that is:
// false = A wins
// true  = B wins
bool runGame(size_t wordLen, Strategy* a, Strategy* b)
{
    Bitstring game;
    while (!gameEnded(game, wordLen))
    {
        Strategy* currPlayer = game.size() % 2 ? b : a;
        game.push_back((*currPlayer)(game, wordLen));
    }
    // At the end of the loop, the losing player has made the final move, and
    // therefore the losing one can be determined by the parity of the game
    // length - if it is even then A and B has made the same number of moves,
    // which means the last player was B since A started, so in this case A has
    // won, but if the parity is odd then B has won.
    return game.size() % 2 == 1;
}

void testStrategies(const std::vector<StrategyInfo>& strategies, size_t n)
{
    Bitstring game;
    std::cout << "Parameters: n=" << n
              << ", symbols=2 (game length upper bound: "
              << (std::pow(2, n)+n) << ")"
              << std::endl;
    for (auto& strategy : strategies)
    {
        auto winMsg = bold() + fgGreen() + "WINNING!" + resetFmt();
        if (strategy.validForA)
        {
            ExtendedProofInfo info{n, 0, 0, {}, &strategy.strategy, false};
            bool win = proveStrategy(&info);
            std::cout << "A-'" << strategy.name << "': ";
            std::cout << (win ? winMsg : "No.");
            std::cout << " " << info << "\n";
        }
        if (strategy.validForB)
        {
            ExtendedProofInfo info{n, 0, 0, {}, &strategy.strategy, true};
            bool win = proveStrategy(&info);
            std::cout << "B-'" << strategy.name << "': ";
            std::cout << (win ? winMsg : "No.");
            std::cout << " " << info << "\n";
        }
    }
    std::cout << std::endl;

    // A basic sanity check: If we somehow end up with a non-empty game
    // after testing our strategies then something adds symbols to the game
    // without removing them afterwards.
    if (!game.empty())
    {
        throw std::runtime_error("Non-empty game after testing!");
    }
}

Strategy createStartStrategy(Bitstring moves)
{
    return [moves](Bitstring& game, size_t)
    {
        size_t move = game.size() / 2;
        if (move >= moves.size()) return false;
        return bool(moves[move]);
    };
}

void findInitialAMoves(size_t n)
{
    if (n%2)
    {
        throw std::runtime_error("A does not have a winning strategy for odd n.");
    }
    Bitstring game;
    // Note that we can assume that A always starts by putting a '0', since the
    // case where a '1' is placed at the beginning is symmetrical (we can just
    // flip the labels of all the symbols).
    size_t maxMoves = std::pow(2, n-1) + n/2;
    std::unique_ptr<bool[]> useAt(new bool[maxMoves]);
    std::vector<Bitstring> validStarts;
    validStarts.push_back({0});
    while (!validStarts.empty())
    {
        Bitstring startMoves = validStarts.back();
        validStarts.pop_back();
        for (size_t i = 0; i < maxMoves; ++i)
        {
            // Initialise such that the parts where the strategy is used is
            // exactly where it is defined.
            useAt[i] = i <= startMoves.size();
        }
        for (size_t symbol = 0; symbol < 2; ++symbol)
        {
            TryPlacing _(startMoves, symbol);
            auto start = createStartStrategy(startMoves);
            bool isWinning = isPartOfWinningStrategy(game, n, &start, 0, useAt.get());
            std::cout << "For n=" << n
                      << ", does A have a winning strategy using initial moves "
                      << startMoves << ": " << (isWinning ? "Yes" : "No")
                      << "\n";
            if (isWinning)
            {
                validStarts.push_back(startMoves);
            }
        }
    }
}

int main()
{
    Bitstring game;
    findInitialAMoves(4);
    findInitialAMoves(6);
    auto strategies = getStrategies();
    testStrategies(strategies, 4);
    Strategy testMe = [](Bitstring& game, size_t)
    {
        if (game.size() < 2) return false;
        size_t move = game.size() / 2;
        bool invert = false;
        if (move == 5) invert = game == "0011001001";
        if (move == 6) invert = game == "011100110110";
        if (move == 7)
        {
            if (game == "00100011001001" || game == "00110010001001" ||
                game == "01100011001001" || game == "01100111001000" ||
                game == "01100111011000") invert = true;
        }
        if (move == 8)
        {
            if (game == "0010011000110111" || game == "0010011100110110" ||
                game == "0110011100100001" || game == "0110011101100001" ||
                game == "0111001101110110" || game == "0111011100110110")
            {
                invert = true;
            }
        }
        if (move == 9)
        {
            if (game == "001001100011011110" ||
                game == "001100100101011101" ||
                game == "001101100011001001" ||
                game == "001101100111001000" ||
                game == "011000100011001001" ||
                game == "011000110010001001" ||
                game == "011001110010000110" ||
                game == "011001110110000110" ||
                game == "011001110111001000" ||
                game == "011101100011001001" ||
                game == "011101100111001000")
            {
                invert = true;
            }

        }
        return invert ? !(move % 2) : !!(move % 2);
    };
    // We wish to test a strategy for n=6. Therefore we should have 2^(n-1)+n/2
    // = 2^5+3 = 35 elements in the array.
    //                      0  1  2  3  4  5  6  7  8  9 10 11 12 13 14 15 16 17
    bool useStrategyAt[] = {1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 0, 0, 0, 0, 0, 0, 0, 0,
    //                     18 19 20 21 22 23 24 25 26 27 28 29 30 31 32 33 34 35
                            0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 1, 1};
    constexpr int debugTurns = 10;
    std::cout << isPartOfWinningStrategy<2*(debugTurns+1)>(game, 6, &testMe, 0, useStrategyAt) << "\n";
}

