Note that in this document it is always assumed that the first move the first player makes is '0'.

For n=4, the following was observed:
* A has a winning strategy.
* A can place at most 9 symbols.
* The first 3 moves are fixed for all winning strategies (does NOT depend on the B's choices).
* The last 3 moves (move 7, 8 and 9) can simply be the opposite of A's previous move.

For n=6, the following was observed:
* A has a winning strategy.
* A can place at most 35 symbols.
* The first 5 moves are fixed.
* The last 4 moves (move 32, 33, 34 and 35) can simply be the opposite of A's previous move.
