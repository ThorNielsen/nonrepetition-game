#include <algorithm>
#include <functional>
#include <iostream>
#include <numeric>
#include <utility>
#include <vector>

// Note: Although this is technically called a "Bitstring", it may in general
// contain more symbols, if that is desired. On most computers it can contain
// symbols in the range [-128, 127].
using Bitstring = std::vector<char>;

// This is custom data type which is used to try to place a symbol and ensuring
// that it is removed again afterwards. It places the symbol at the time of
// declaration (being written in the code) and it removes the symbol when it
// reaches a '}' which corresponds to the '{' immediately preceding the
// declaration.
class TryPlacing
{
public:
    // This is called upon construction of an element of this type and simply
    // adds the given symbol to the bitstring and makes a note of where it added
    // that element.
    TryPlacing(Bitstring& bs, Bitstring::value_type val)
    : bs_(&bs)
    {
        bs.push_back(val);
    }

    // This is called when an element of this type is destroyed, that is
    // whenever the element is not visible anymore. That could be at the end of
    // a function (preceding a return), at the end of a loop, etc.
    ~TryPlacing()
    {
        bs_->pop_back();
    }
private:
    Bitstring* bs_;
};

// A strategy should take the current game and word length, and return the
// chosen symbol.
using Strategy = std::function<bool(Bitstring&, size_t)>;

struct StrategyInfo
{
    Strategy strategy;
    std::string name;
    bool validForA;
    bool validForB;
};

void enableFormatting();
void disableFormatting();
bool formattingEnabled();

std::string fgRed();
std::string fgGreen();
std::string fgYellow();
std::string fgBlue();
std::string fgPurple();
std::string fgCyan();
std::string bold();
std::string resetFmt();

inline std::ostream& operator<<(std::ostream& ost, const Bitstring& bs)
{
    std::string colours[2] = {fgGreen(), fgBlue()};
    size_t player = 0;
    for (auto& b : bs)
    {
        ost << colours[(player++) % 2];
        ost << (int)b;
    }
    ost << resetFmt();
    return ost;
}

inline bool operator==(const Bitstring& bs, const std::string& s)
{
    if (bs.size() != s.size()) return false;
    for (size_t i = 0; i < bs.size(); ++i)
    {
        if (bs[i]+'0' != s[i]) return false;
    }
    return true;
}

inline bool operator==(const std::string& s, const Bitstring& bs)
{
    return bs == s;
}

inline bool operator!=(const Bitstring& bs, const std::string& s)
{
    return !(bs == s);
}

inline bool operator!=(const std::string& s, const Bitstring& bs)
{
    return !(bs == s);
}

inline bool beginsWith(const Bitstring& bs, const std::string& s)
{
    if (bs.size() < s.size()) return false;
    for (size_t i = 0; i < s.size(); ++i)
    {
        if (bs[i]+'0' != s[i]) return false;
    }
    return true;
}

// This function tests whether the given game has just ended (that is, whether
// the last move has caused a game end). It does NOT work if the game has been
// continued - e.g. if n=1, game=00 will return true while n=1, game=001 may
// return anything.
bool gameEnded(const Bitstring& game, size_t wordLen);

// Test whether a given move causes the game to end.
inline bool causesGameEnd(bool move, Bitstring& game, size_t wordLen)
{
    TryPlacing guard(game, move);
    return gameEnded(game, wordLen);
}

std::vector<StrategyInfo> getStrategies();

const StrategyInfo* findStrategy(std::string name,
                                 const std::vector<StrategyInfo>& strategies);

